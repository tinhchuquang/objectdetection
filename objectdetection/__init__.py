# from .train_augumentations import train
from .train import train
from .load_data import loaddata, load_voc
from .datav2 import Data
from .results import test

from .augmentations import SSDAugmentation
