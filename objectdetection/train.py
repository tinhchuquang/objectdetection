from .augmentations import SSDAugmentation
from .multibox_loss import MultiBoxLoss
from .ssd import build_ssd
from .voc0712 import VOC_ROOT, VOCDetection
from .coco import COCO_ROOT, COCODetection
from .config import data_CMT, voc, coco, MEANS
from .load_default import detection_collate
from .data import Data


import parser
import os
import time
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.optim as optim
import torch.backends.cudnn as cudnn
import torch.nn.init as init
import torch.utils.data as data


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

args = {
    'dataset': 'VOC',
    'dataset_root': VOC_ROOT,
    'basenet': 'vgg16_reducedfc.pth',
    'batch_size': 32,
    'resume': False,
    'start_iter': 0,
    'num_workers': 4,
    'cuda': True,
    'learning-rate': 1e-3,
    'momentum': 0.9,
    'weight_decay': 5e-4,
    'gamma': 0.1,
    'visdom': False,
    'save_folder': './objectdetection/weights/',
}

if args['visdom']:
    import visdom

    viz = visdom.Visdom()

if torch.cuda.is_available():
    if args['cuda']:
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
    if not args['cuda']:
        print("WARNING: It looks like you have a CUDA device, but aren't " +
              "using CUDA.\nRun with --cuda for optimal training speed.")
        torch.set_default_tensor_type('torch.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')

if not os.path.exists(args['save_folder']):
    os.mkdir(args['save_folder'])


def train():
    if args['dataset'] == 'COCO':
        if args['dataset_root'] == VOC_ROOT:
            if not os.path.exists(COCO_ROOT):
                parser.error('Must specify dataset_root if specifying dataset')
            print("WARNING: Using default COCO dataset_root because " +
                  "--dataset_root was not specified.")
            args['dataset_root'] = COCO_ROOT
        cfg = coco
        dataset = COCODetection(root=args['dataset_root'],
                                transform=SSDAugmentation(cfg['min_dim'],
                                                          MEANS))
    elif args['dataset'] == 'VOC':
        if args['dataset_root'] == COCO_ROOT:
            parser.error('Must specify dataset if specifying dataset_root')
        cfg = voc
        dataset = VOCDetection(root=args['dataset_root'],
                               transform=SSDAugmentation(cfg['min_dim'],
                                                         MEANS))
    else:
        cfg = data_CMT
        dataset = Data(args['dataset_root'], transform=None)

    ssd_net = build_ssd('train', cfg['min_dim'], cfg['num_classes'])
    net = ssd_net

    if args['cuda']:
        net = torch.nn.DataParallel(ssd_net)
        cudnn.benchmark = True

    if args['resume']:
        print('Resuming training, loading {}...'.format(args['resume']))
        ssd_net.load_weights(args['resume'])
    else:
        vgg_weights = torch.load(args['save_folder'] + args['basenet'])
        print('Loading base network...')
        ssd_net.vgg.load_state_dict(vgg_weights)

    if args['cuda']:
        net = net.cuda()

    if not args['resume']:
        print('Initializing weights...')
        ssd_net.extras.apply(weights_init)
        ssd_net.loc.apply(weights_init)
        ssd_net.conf.apply(weights_init)

    optimizer = optim.SGD(net.parameters(), lr=args['learning-rate'], momentum=args['momentum'],
                          weight_decay=args['weight_decay'])
    criterion = MultiBoxLoss(cfg['num_classes'], 0.5, True, 0, True, 3, 0.5,
                             False, args['cuda'])

    net.train()
    loc_loss = 0
    conf_loss = 0
    epoch = 0
    print('Loading the dataset...')

    epoch_size = len(dataset) // args['batch_size']
    print('Training SSD on:', dataset.name)
    print('Using the specified args:')
    print(args)

    step_index = 0

    if args['visdom']:
        vis_title = 'SSD.PyTorch on ' + dataset.name
        vis_legend = ['Loc Loss', 'Conf Loss', 'Total Loss']
        iter_plot = create_vis_plot('Iteration', 'Loss', vis_title, vis_legend)
        epoch_plot = create_vis_plot('Epoch', 'Loss', vis_title, vis_legend)

    data_loader = data.DataLoader(dataset, args['batch_size'],
                                  num_workers=args['num_workers'],
                                  shuffle=True, collate_fn=detection_collate,
                                  pin_memory=True)

    for iteration in range(args['start_iter'], cfg['max_iter']):
        print('Epoch : ', iteration)
        for index, (images, targets) in enumerate(data_loader):
            if args['visdom'] and iteration != 0 and (iteration % epoch_size == 0):
                update_vis_plot(epoch, loc_loss, conf_loss, epoch_plot, None,
                                'append', epoch_size)
                loc_loss = 0
                conf_loss = 0
                epoch += 1

            if iteration in cfg['lr_steps']:
                step_index += 1
                adjust_learning_rate(optimizer, args['gamma'], step_index)


            if args['cuda']:
                images = Variable(images.cuda())
                with torch.no_grad():
                    targets = [Variable(ann.cuda()) for ann in targets]
            else:
                images = Variable(images)
                with torch.no_grad:
                    targets = [Variable(ann) for ann in targets]
            t0 = time.time()
            out = net(images)
            optimizer.zero_grad()
            loss_l, loss_c = criterion(out, targets)
            loss = loss_l + loss_c
            loss.backward()
            optimizer.step()
            t1 = time.time()
            loc_loss += loss_l.data
            conf_loss += loss_c.data

            if (index) % 10 == 0:
                print('iter ' + repr(index) + ' || Loss: %.4f ||' % (loss.data) +
                      'timer: %.4f sec.' % (t1 - t0))

            if args['visdom']:
                update_vis_plot(iteration+index, loss_l.data, loss_c.data,
                                iter_plot, epoch_plot, 'append')

            if (iteration) != 0 and (iteration ) % 5 == 0:
                torch.save(ssd_net.state_dict(), 'weights/ssd300_COCO_' +
                           repr(iteration) + '.pth')
        torch.save(ssd_net.state_dict(),
                   args['save_folder'] + '' + args['dataset'] + '.pth')


def adjust_learning_rate(optimizer, gamma, step):
    lr = args.lr * (gamma ** (step))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def xavier(param):
    init.xavier_uniform_(param)


def weights_init(m):
    if isinstance(m, nn.Conv2d):
        xavier(m.weight.data)
        m.bias.data.zero_()


def create_vis_plot(_xlabel, _ylabel, _title, _legend):
    return viz.line(
        X=torch.zeros((1,)).cpu(),
        Y=torch.zeros((1, 3)).cpu(),
        opts=dict(
            xlabel=_xlabel,
            ylabel=_ylabel,
            title=_title,
            legend=_legend
        )
    )


def update_vis_plot(iteration, loc, conf, window1, window2, update_type,
                    epoch_size=1):
    viz.line(
        X=torch.ones((1, 3)).cpu() * iteration,
        Y=torch.Tensor([loc, conf, loc + conf]).unsqueeze(0).cpu() / epoch_size,
        win=window1,
        update=update_type
    )
    if iteration == 0:
        viz.line(
            X=torch.zeros((1, 3)).cpu(),
            Y=torch.Tensor([loc, conf, loc + conf]).unsqueeze(0).cpu(),
            win=window2,
            update=True
        )
