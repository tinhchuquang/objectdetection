import torch.utils.data as data
import cv2
import numpy as np
import os.path as osp
import torch

from .config import HOME

DATA_CLASSES = (

)

DATA_ROOT = osp.join(HOME+ 'data/')


class DataAnnomationTransform(object):

    def __int__(self):
        pass

    def __call__(self, boxs, labels, height, width):
        scales = np.array([width, height, width, height])
        res = []
        for index, (box, label) in enumerate(zip(boxs, labels)):
            box = list(np.array(box, np.float) / scales)
            res += [box + [label]]
        return res


class Data(data.Dataset):

    def __init__(self, paths, transform=None, target_transform=DataAnnomationTransform()):
        self.name = 'data_cmt'
        with open(paths, 'r') as f:
            datas = f.readlines()
        self.images = []
        self.boxes = []
        self.labels = []
        for data in datas:
            data = data.strip()
            data = data.split(' ')
            image = cv2.imread(data[0])
            self.images.append(image)
            box = []
            label = []
            for element in data[1:]:
                element = element.split(',')
                box.append([int(element[0]), int(element[1]), int(element[2]), int(element[3])])
                label.append(int(element[4]))
            self.boxes.append(box)
            self.labels.append(label)
        self.target_transform = target_transform
        self.transform = transform

    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        image, boxs, label = self.pull_item(index)
        target = np.hstack((boxs, np.expand_dims(label, axis=1)))
        return torch.FloatTensor(image).permute(2, 0, 1), target

    def pull_item(self, index):
        targets = None
        h, w, _ = self.images[index].shape
        if self.target_transform is not None:
            targets = self.target_transform(self.boxes[index], self.labels[index], h, w)
        if self.transform is not None:
            targets = np.array(targets)
            image, boxs, label = self.transform(self.images[index], targets[:, :4], targets[:, 4])
            image = image[:, :, (2, 1, 0)]

        return image, boxs, label

    def pull_image(self, index):
        return self.images[index]

    def pull_anno(self, index):
        targets = None
        h, w, _ = self.images[index].shape
        if self.target_transform is not None:
            targets = self.target_transform(self.boxes[index], self.labels[index], h, w)
        return index, targets