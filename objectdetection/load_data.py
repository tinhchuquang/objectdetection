from .datav2 import Data
from .augmentations import SSDAugmentation
import torch.utils.data as data
from .config import data_CMT, MEANS, voc
from .load_default import detection_collate
from .voc0712 import VOC_ROOT, VOCDetection

import numpy as np

def loaddata():

    args = {
        'dataset': 'data',
        'batch_size': 32,
        'dataset_root': './objectdetection/train.txt',
        'num_workers': 4
    }

    cfg = data_CMT
    dataset = Data(args['dataset_root'], transform=SSDAugmentation(cfg['min_dim'], MEANS))
    data_loader = data.DataLoader(dataset, args['batch_size'],
                                  num_workers=args['num_workers'],
                                  shuffle=True, collate_fn=detection_collate,
                                  pin_memory=True)
    image_greater_255 = 0
    image_smaller_0 = 0
    for index, (images, targets) in enumerate(data_loader):
        for i in range(images.shape[0]):
            if images[i][np.where(images[i] > 255)].shape[0] != 0:
                image_greater_255 += 1
            if images[i][np.where(images[i] < 0)].shape[0] != 0:
                image_smaller_0 += 1
    print("Image greater 255 : ", image_greater_255)
    print('Image smaller 0: ', image_smaller_0)


def load_voc():

    args = {
        'dataset': 'VOC',
        'batch_size': 32,
        'dataset_root': VOC_ROOT,
        'num_workers': 4
    }

    cfg = voc
    dataset = VOCDetection(args['dataset_root'], transform=SSDAugmentation(cfg['min_dim'], MEANS))
    data_loader = data.DataLoader(dataset, args['batch_size'],
                                  num_workers=args['num_workers'], shuffle=True,
                                  collate_fn=detection_collate, pin_memory=True)
    image_greater_255 = 0
    image_smaller_0 = 0
    for index, (images, targets) in enumerate(data_loader):
        for i in range(images.shape[0]):
            if images[i][np.where(images[i] > 255)].shape[0] != 0:
                image_greater_255 += 1
            if images[i][np.where(images[i] < 0)].shape[0] != 0:
                image_smaller_0 += 1
    print("Image greater 255 : ", image_greater_255)
    print('Image smaller 0: ', image_smaller_0)
