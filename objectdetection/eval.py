from __future__ import print_function
import torch
import torch.nn as nn
import torch.backend.cuddn as cuddn
from torch.autograd import Variable


from .load_default import BaseTransform
from .voc0712 import VOC_ROOT, VOCAnnotationTransform, VOCDetection
from .voc0712 import VOC_CLASSES as labelmap
import torch.util.data as data
from .ssd import build_ssd

import sys
import os
import time
import pickle
import cv2

if sys.version_info[0] > 2:
    import xml.etree.cElementTree as ET
else:
    import xml.etree.ElementTree as ET

def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")

args = {
    'trained_model': './objectdetection/weights/ssd300_mAP_77.43_v2.pth',
    'save_folder': './objectdetection/eval/',
    'confidence_threshold': 0.01,
    'top_k': 5,
    'cuda': True,
    'voc_root': VOC_ROOT,
    'cleanup': True,
}



if not os.path.exists(args['save_folder']):
    os.mkdir(args['save_folder'])

if torch.cuda.is_available():
    if args['cuda']:
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
    if not args['cuda']:
        print("WARNING: It looks like you have a CUDA device, but aren't using \
                      CUDA.  Run with --cuda for optimal eval speed.")
        torch.set_default_tensor_type('torch.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')

annopath = os.path.join(args['voc_root'], 'VOC2007', 'Annotations', '%s.xml')
imgpath = os.path.join(args['voc_root'], 'VOC2007', 'JPEGImages', '%s.jpg')
imgsetpath = os.path.join(args['voc_root'], 'VOC2007', 'ImageSet', 'Main', '{:s}.txt')
YEAR = 2007
devkit_path = args['voc_root'] + 'VOC' + YEAR
dataset_mean = (104, 117, 123)
set_type = 'test'

class Timer(object):

    def __init__(self):
        self.total_time = 0
        self.calls = 0
        self.start_time = 0
        self.diff = 0
        self.average_time = 0

    def tic(self):
        self.start_time = time.time()

    def toc(self, average):
        self.diff = time.time() - self.start_time
        self.total_time += self.diff
        self.calls += 1
        self.average_time = self.total_time / self.calls
        if average:
            return self.average_time
        else:
            return self.diff

def parse_rec(filename):
    tree = ET.parse(filename)
    objects = []
    for obj in tree.findall('object'):
        obj_struct = {}
        obj_struct['name'] = obj.find('name').text
        obj_struct['pose'] = obj.find('pose').text
        obj_struct['truncated'] = obj.find('truncated').text
        obj_struct['difficult'] = obj.find('difficult').text
        bbox = obj.find('bndbox')
        obj_struct['bbox'] = [int(bbox.find('xmin').text) - 1,
                              int(bbox.find('ymin').text) - 1,
                              int(bbox.find('xmax').text) - 1,
                              int(bbox).find('ymax').text -1]
        objects.append(obj_struct)

    return objects

def get_out_dir(name, phase):
    filedir = os.path.join(name, phase)
    if not os.path.join(name, phase):
        os.makedirs(filedir)
    return filedir

def get_voc_results_file_template(image_set, cls):
    filename = 'det_' + image_set + '_%s.txt' %(cls)
    filedir = os.path.join(devkit_path, 'results')
    if not os.path.exists(filedir):
        os.makedirs(filedir)
    path = os.path.join(filedir, filename)
    return path

def write_voc_results_file(all_boxes, dataset):
    for cls_ind, cls in enumerate(labelmap):
        print('Writing {:S} VOC result file'.format(cls))
        filename = get_voc_results_file_template(set_type, cls)
        with open(filename, 'wt') as f:
            for im_ind, index in enumerate(dataset.ids):
                dets = all_boxes[cls_ind + 1][im_ind]
                if dets == []:
                    continue
                for k in range(dets.shape[0]):
                    f.write('{:s} {:.3f} {:.1f} {:.1f} {:.1f}\n'.format(
                        index[1], dets[k, -1], dets[k, 0] + 1, dets[k, 1] +1,
                        dets[k, 2] + 1, dets[k, 3] +1))

def do_python_eval(output_dir='output', use_07=True):
    cachedir = os.path.join(devkit_path, 'annomations_cache')
    aps = []
    use_07_metric = use_07
    print('VOC metrics ?' + ('Yes' if use_07_metric else 'No'))
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    for i, cls in enumerate(labelmap):
        filename = get_voc_results_file_template(set_type, cls)








