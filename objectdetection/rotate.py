import cv2
import numpy as np

theta = -60
def rotate_bound(image, angle):
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    M = cv2.getRotationMatrix2D((cX, cY), angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])


    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    return cv2.warpAffine(image, M, (nW, nH))

def rotate_coord(coord, cx, cy, h, w):
    M = cv2.getRotationMatrix2D((cx, cy), theta, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    M[0, 2] += (nW / 2) - cx
    M[1, 2] += (nH / 2) - cy
    v = [coord[0],coord[1],1]
    calculated = np.dot(M,v)
    return calculated[0], calculated[1]

if __name__ == '__main__':
    x, y = 10, 20
    image = cv2.imread('/home/tinh/1.jpeg')
    image_new = rotate_bound(image, theta)
    (heigth, width) = image.shape[:2]
    (cX, cY) = (width // 2, heigth // 2)
    x, y = rotate_coord((x, y), cX, cY, heigth, width)
    x = int(x)
    y = int(y)
    cv2.circle(image_new, (x, y), 3, (0, 255, 0), -1)
    cv2.imshow('image',image_new)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
