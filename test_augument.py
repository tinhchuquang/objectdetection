from objectdetection import *
import cv2
import numpy as np

def show_image(image, boxes, name='image_convert'):
    for box in boxes:
        image = cv2.rectangle(image, (box[0], box[1]), (box[2], box[3]), (0, 255, 0), 3)
    cv2.imshow(name, image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def augumentation(path, targets, labels):
    image = cv2.imread(path)
    height, width, _ = image.shape
    scale = np.array([width, height, width, height])
    image = image[:, :, (2, 1, 0)]
    show_image(image, targets)
    for index, (target, label) in enumerate(zip(targets, labels)):
        targets[index] = list(np.array(target, np.float))
        targets[index] = list(np.array(target) / scale)
        targets[index] += [label]
    targets = np.array(targets)
    image_result, boxes, labels = SSDAugmentation()(image, targets[:, :4], targets[:, 4])
    height, width, _ = image_result.shape
    scale = [width, height, width, height]
    for i, box in enumerate(boxes):
        boxes[i] = list(np.array(box * scale))
    boxes = list(np.array(boxes, np.uint))
    image_result = np.array(image_result, np.uint8)
    show_image(image_result, boxes, name='new')

if __name__ == '__main__':
    with open('./objectdetection/train.txt') as f:
        datas = f.readlines()
    for data in datas:
        data = data.strip()
        data = data.split(' ')
        image_path = data[0]
        box = []
        label = []
        for element in data[1:]:
            element = element.split(',')
            box.append([int(element[0]), int(element[1]), int(element[2]), int(element[3])])
            label.append(int(element[4]))
        augumentation(image_path, box, label)
