from __future__ import print_function
import os

from .ssd import build_ssd
from .datav2 import Data
from .load_default import BaseTransform

import torch
import torch.backends.cudnn as cudnn
from torch.autograd import Variable

args = {
    'trained_model': './objectdetection/weights/best_model.pth',
    'basenet': 'vgg16_reducedfc.pth',
    'cuda': True,
    'save_folder': './objectdetection/eval/',
    'visual_threshold': 0.6,

}

if args['cuda'] and torch.cuda.is_available():
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    torch.set_default_tensor_type('torch.FloatTensor')

if not os.path.exists(args['save_folder']):
    os.makedirs(args['save_folder'])

def test_net(save_folder, net, cuda, testset, transform, thresh):
    filename = save_folder+'test1.txt'
    num_images = len(testset)
    for i in range(num_images):
        print('Testing image {:d}/{:d}....'.format(i+1, num_images))
        img = testset.pull_image(i)
        img_id, annotation = testset.pull_anno(i)
        x = torch.from_numpy(transform(img)[0]).permute(2, 0, 1)
        x = Variable(x.unsqueeze(0))

        with open(filename, mode='a') as f:
            f.write('\nGROUND TRUTH FOR: '+img_id+'\n')
            for box in annotation:
                f.write('label: '+' || '.join(str(b) for b in box)+'\n')
        if cuda:
            x = x.cuda()

        y = net(x)
        detections = y.data
        scale = torch.Tensor([img.shape[1], img.shape[0],
                             img.shape[1], img.shape[0]])
        pred_num = 0
        print(detections)
        for i in range(detections.size(1)):
            j = 0
            while detections[0, i, j, 0] >= thresh:
                if pred_num == 0:
                    with open(filename, mode='a') as f:
                        f.write('PREDICTIONS: '+'\n')
                score = detections[0, i, j, 0]
                label_name = str(i-1)
                # pt = (detections[0, i, j, 1:]*scale).cpu().numpy()
                pt = (detections[0, i, j, 1:]).cpu().numpy()
                coords = (pt[0], pt[1], pt[2], pt[3])
                pred_num += 1
                with open(filename, mode='a') as f:
                    f.write(str(pred_num)+' label: '+label_name+' score: ' +
                            str(score) + ' '+' || '.join(str(c) for c in coords) + '\n')
                j += 1


def test():
    num_classes = 4
    net = build_ssd('test', 300, num_classes)
    net.load_state_dict(torch.load(args['trained_model']))
    net.eval()
    print('Finished loading model!')
    testset = Data(paths='./objectdetection/test.txt')
    if args['cuda']:
        net = net.cuda()
        cudnn.benchmark = True
    test_net(args['save_folder'], net, args['cuda'], testset,
             BaseTransform(net.size, (104, 117, 123)),
             thresh=args['visual_threshold'])