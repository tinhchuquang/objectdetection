import torch.utils.data as data
import cv2
import numpy as np
import os.path as osp
import torch

from .config import HOME

DATA_CLASSES = (

)

DATA_ROOT = osp.join(HOME+ 'data/')


class Data(data.Dataset):

    def __init__(self, paths, transform=None, target_transform=True):
        self.name = 'data cmt'
        with open(paths, 'r') as f:
            datas = f.readlines()
        self.images = []
        self.boxes = []
        self.labels = []
        for data in datas:
            data = data.strip()
            data = data.split(' ')
            image = cv2.imread(data[0])
            w, h, _ = image.shape
            image = cv2.resize(image, (300, 300))
            self.images.append(image)
            box = []
            label = []
            for element in data[1:]:
                element = element.split(',')
                box.append([int(element[0]), int(element[1]), int(element[2]), int(element[3])])
                label.append(int(element[4]))
            self.boxes.append(box)
            self.labels.append(label)
        if target_transform:
            self.transform(w, h, 300, 300)
        if transform is not None:
            for index, _ in enumerate(self.boxes):
                self.boxes[index] = np.array(self.boxes[index])
                self.labels[index] = np.array(self.labels[index])
                self.images[index], self.boxes[index], self.labels[index] \
                    = transform(self.images[index], self.boxes[index], self.labels[index])
                self.images[index] = self.images[index][:, :, (2, 1, 0)]


    def __len__(self):
        return len(self.images)

    def __getitem__(self, index):
        target = np.hstack((self.boxes[index], np.expand_dims(self.labels[index], axis=1)))
        return torch.FloatTensor(self.images[index]).permute(2, 0, 1), target

    def transform(self, w_default, h_default, w_new, h_new):
        pts = ['xmin', 'ymin', 'xmax', 'ymax']
        for index, boxs in enumerate(self.boxes):
            for k, element in enumerate(boxs):
                for i, pt in enumerate(pts):
                    if pt.startswith('x'):
                        self.boxes[index][k][i] = int(int(self.boxes[index][k][i]) * h_new / h_default)
                    else:
                        self.boxes[index][k][i] = int(int(self.boxes[index][k][i]) * w_new / w_default)
            

